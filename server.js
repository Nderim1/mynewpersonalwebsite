

const express = require("express");
const app = express();
const router = express.Router();
const nunjucks = require('nunjucks')
const path = require('path')


nunjucks.configure('views', {
    autoescape: true,
    express: app
})


app.use(express.static(path.join(__dirname, 'public')))

router.use(function (req,res,next) {
  console.log("/" + req.method);
  next();
});
// console.log(path)
router.get("/", (req,res) => {
  res.render("index.html")
});



// router.get("/about",function(req,res){
//   res.sendFile(path + "about.html");
// });

// router.get("/contact",function(req,res){
//   res.sendFile(path + "contact.html");
// });

app.use("/",router);

app.use("*",function(req,res){
  res.sendFile(path + "404.html");
});


app.listen(3000,function(){
  console.log("Live at Port 3000");
});