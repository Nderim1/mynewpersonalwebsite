$(document).ready(function(){
    $(".main").onepage_scroll({
        sectionContainer: "section", 
        easing: "ease"
    })

    $('#discoverLink').click(function(){
       $(".main").moveTo(2)
        // $('html, body').animate({
        //     scrollTop: $( $(this).attr('href') ).offset().top
        // }, 500);
        // return false;
    });

    $('.contactItems').hover(function(){
        $(this).css('cursor', 'pointer')
        $(this).addClass('onHover')
        $('i[class^="fa fa-"]', this).addClass('fa-3x')
    }, function() {
        $( this ).removeClass( "onHover")
        $('i[class^="fa fa-"]', this).removeClass('fa-3x')
    })

    $('.contactItems').click(function(){
        switch (event.currentTarget.id) {
            case 'linkedin':
                window.open("https://www.linkedin.com/in/nderim-topalli/")
                break;
            case 'bitbucket':
                window.open("https://bitbucket.org/Nderim1/")
                break;
            case 'github':
                window.open("https://github.com/Nderim1")
                break;
            case 'facebook':
                window.open("http://linkedin.com")
                break;
        }
        
    })
    var map = L.map('map').setView([46.0710668, 13.2345], 13)
    
    L.tileLayer("http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png", {
        attribution: "&copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> &copy; <a href='http://cartodb.com/attributions'>CartoDB</a>"
    }).addTo(map)
    L.marker([46.06399, 13.2345]).addTo(map)
})